import styled from 'styled-components'

export const Button = styled.button`
  color: #fff;
  background: none;
  border: none;
  cursor: pointer;
  font-size: 14px;

  &:disabled {
    cursor: not-allowed;
    color: #eee;
  }
`
