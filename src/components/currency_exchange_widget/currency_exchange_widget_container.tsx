import React from 'react'
import styled from 'styled-components'
import { observer } from 'mobx-react'
import { useCurrencyExchangeWidgetStore } from '../../contexts/currency_exchange_widget_store_context'
import { Button } from '../ui/button'
import { Slider } from './slider/slider'
import { Description } from '../ui/typography'
import { EMode } from '../../types/mode'

const Container = styled.div`
  background: #3f94e4;
  width: 414px;
  height: 496px;
  padding-top: 24px;

  .container {
    padding: 0 8px;
  }

  .height-full {
    height: 100%;
  }

  .text-right {
    text-align: right;
  }

  .mts {
    margin-top: 8px;
  }

  .mtm {
    margin-top: 16px;
  }
`

const Header = styled.header`
  display: flex;
  justify-content: space-between;
`

const ErrorContainer = styled.div`
  position: relative;
  width: 100%;
  height: 24px;
  margin-top: 16px;
`

const ErrorWrapper = styled.div`
  background-color: coral;
  width: 100%;
  text-align: center;
  position: absolute;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  font-size: 14px;
`

export const CurrencyExchangeWidgetContainer = observer(() => {
  const { currencyExchangeWidgetStore } = useCurrencyExchangeWidgetStore()

  if (!currencyExchangeWidgetStore.networkStatus.isLoaded) {
    return null
  }

  const handleFocus = (mode: EMode) => () => {
    currencyExchangeWidgetStore.updateActiveMode(mode)
  }

  return (
    <Container data-testid="ac-currency-exchange-widget-container">
      <div className="container">
        <Header>
          <Button disabled>Cancel</Button>
          <Description data-testid="ac-currency-exchange-slide-account-from-rate">
            {currencyExchangeWidgetStore.formattedAccountFromRate}
          </Description>
          <Button
            data-testid="ac-currency-exchange-widget-exchange-button"
            onClick={currencyExchangeWidgetStore.exchange}
            disabled={currencyExchangeWidgetStore.isError}
          >
            Exchange
          </Button>
        </Header>
      </div>
      <div>
        <ErrorContainer>
          {currencyExchangeWidgetStore.isError && (
            <ErrorWrapper data-testid="ac-currency-exchange-widget-error">{currencyExchangeWidgetStore.errors}</ErrorWrapper>
          )}
        </ErrorContainer>
        <div>
          <Slider
            mode={EMode.from}
            accounts={currencyExchangeWidgetStore.accounts}
            onChangeSlide={currencyExchangeWidgetStore.updateActiveFromAccount}
            activeAccountCurrency={
              currencyExchangeWidgetStore.activeAccountFromCurrency
            }
            onFocus={handleFocus(EMode.from)}
            isFocused={currencyExchangeWidgetStore.activeMode === EMode.from}
          />
        </div>
        <Slider
          key={currencyExchangeWidgetStore.key}
          mode={EMode.to}
          accounts={currencyExchangeWidgetStore.accountsTo}
          onChangeSlide={currencyExchangeWidgetStore.updateActiveToAccount}
          activeAccountCurrency={
            currencyExchangeWidgetStore.activeAccountToCurrency
          }
          onFocus={handleFocus(EMode.to)}
          isFocused={currencyExchangeWidgetStore.activeMode === EMode.to}
        />
      </div>
    </Container>
  )
})
