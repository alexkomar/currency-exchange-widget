import '../../matchMedia'
import 'raf/polyfill'

import { screen, waitFor, cleanup, within } from '@testing-library/react'

import { renderCurrencyExchangeWidget, prepareWidget } from './helpers'
import { AccountsStore } from '../../../stores/accounts_store'
import { FakeFetcher } from '../../../stores/helpers/fake_fetcher'
import userEvent from '@testing-library/user-event'

jest.useRealTimers()

describe('CurrencyExchangeWidget', () => {
  let accountsStore
  let fetcher

  beforeEach(() => {
    accountsStore = AccountsStore.create({
      accounts: [
        { currency: 'USD', sum: 100 },
        { currency: 'EUR', sum: 0 },
        { currency: 'GBP', sum: 0 },
        { currency: 'UAH', sum: 0 },
      ],
    })

    fetcher = new FakeFetcher({ randomizeRates: false })
  })

  afterEach(() => {
    cleanup()
  })

  it(`should render widget`, async () => {
    await renderCurrencyExchangeWidget({ accountsStore, fetcher })

    expect(
      await screen.findByTestId('ac-currency-exchange-widget-container')
    ).toBeInTheDocument()
  })

  describe(`should render available accounts sliders amount`, () => {
    it(`should be 4 slides in the from slider`, async () => {
      const { fromAccountSlider } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(
        Array.from(
          await within(fromAccountSlider).findAllByTestId(
            'ac-currency-exchange-widget-slider-slide'
          )
        ).length
      ).toMatchInlineSnapshot(`4`)
    })

    it(`should be 3 slides in the from slider`, async () => {
      const { toAccountSlider } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(
        Array.from(
          await within(toAccountSlider).findAllByTestId(
            'ac-currency-exchange-widget-slider-slide'
          )
        ).length
      ).toMatchInlineSnapshot(`3`)
    })
  })

  describe(`should remove inappropriate slides`, () => {
    it(`USD is default`, async () => {
      const { toAccountSlider, fromAccountSlider } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(within(fromAccountSlider).getByText('USD')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('EUR')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('GBP')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('UAH')).toBeInTheDocument()

      expect(within(toAccountSlider).queryByText('USD')).not.toBeInTheDocument()
      expect(within(toAccountSlider).getByText('EUR')).toBeInTheDocument()
      expect(within(toAccountSlider).getByText('GBP')).toBeInTheDocument()
      expect(within(toAccountSlider).getByText('UAH')).toBeInTheDocument()
    })

    it(`EUR is default`, async () => {
      accountsStore = AccountsStore.create({
        accounts: [
          { currency: 'EUR', sum: 0 },
          { currency: 'USD', sum: 100 },
          { currency: 'GBP', sum: 0 },
          { currency: 'UAH', sum: 0 },
        ],
      })

      const { toAccountSlider, fromAccountSlider } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(within(fromAccountSlider).getByText('USD')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('EUR')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('GBP')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('UAH')).toBeInTheDocument()

      expect(within(toAccountSlider).getByText('USD')).toBeInTheDocument()
      expect(within(toAccountSlider).queryByText('EUR')).not.toBeInTheDocument()
      expect(within(toAccountSlider).getByText('GBP')).toBeInTheDocument()
      expect(within(toAccountSlider).getByText('UAH')).toBeInTheDocument()
    })

    it(`GBP is default`, async () => {
      accountsStore = AccountsStore.create({
        accounts: [
          { currency: 'GBP', sum: 0 },
          { currency: 'EUR', sum: 0 },
          { currency: 'USD', sum: 100 },
          { currency: 'UAH', sum: 0 },
        ],
      })

      const { toAccountSlider, fromAccountSlider } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(within(fromAccountSlider).getByText('USD')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('EUR')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('GBP')).toBeInTheDocument()
      expect(within(fromAccountSlider).getByText('UAH')).toBeInTheDocument()

      expect(within(toAccountSlider).getByText('USD')).toBeInTheDocument()
      expect(within(toAccountSlider).getByText('EUR')).toBeInTheDocument()
      expect(within(toAccountSlider).queryByText('GBP')).not.toBeInTheDocument()
      expect(within(toAccountSlider).getByText('UAH')).toBeInTheDocument()
    })
  })

  describe(`USD to EUR`, () => {
    let accountsStore = AccountsStore.create({
      accounts: [
        { currency: 'USD', sum: 100 },
        { currency: 'EUR', sum: 0 },
        { currency: 'GBP', sum: 0 },
        { currency: 'UAH', sum: 0 },
      ],
    })

    it(`should show accountFrom rate`, async () => {
      const { accountFromRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromRate.textContent).toMatchInlineSnapshot(`"$1= €0,8471"`)
    })

    it(`should show accountTo rate`, async () => {
      const { accountToRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountToRate.textContent).toMatchInlineSnapshot(`"€1= $1.18"`)
    })

    it(`should show accountFrom sum`, async () => {
      const { accountFromSum } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromSum.textContent).toMatchInlineSnapshot(
        `"You have $100"`
      )
    })

    it(`should be empty inputs by default`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(inputFrom.value).toMatchInlineSnapshot(`""`)
      expect(inputTo.value).toMatchInlineSnapshot(`""`)
    })

    it(`should calculate value from USD to GBP and GBP to USD`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      userEvent.type(inputFrom, '10')

      expect(inputFrom.value).toMatchInlineSnapshot(`"-10"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+8.47"`)

      userEvent.clear(inputTo)
      userEvent.type(inputTo, '8')

      await waitFor(() => expect(document.activeElement).toEqual(inputTo))

      expect(inputFrom.value).toMatchInlineSnapshot(`"-9.44"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+8"`)
    })
  })

  describe(`USD to GBP`, () => {
    let accountsStore = AccountsStore.create({
      accounts: [
        { currency: 'USD', sum: 100 },
        { currency: 'GBP', sum: 0 },
        { currency: 'EUR', sum: 0 },
        { currency: 'UAH', sum: 0 },
      ],
    })

    it(`should show accountFrom rate`, async () => {
      const { accountFromRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromRate.textContent).toMatchInlineSnapshot(`"$1= £0.7260"`)
    })

    it(`should show accountTo rate`, async () => {
      const { accountToRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountToRate.textContent).toMatchInlineSnapshot(`"£1= $1.38"`)
    })

    it(`should show accountFrom sum`, async () => {
      const { accountFromSum } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromSum.textContent).toMatchInlineSnapshot(
        `"You have $100"`
      )
    })

    it(`should be empty inputs by default`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(inputFrom.value).toMatchInlineSnapshot(`""`)
      expect(inputTo.value).toMatchInlineSnapshot(`""`)
    })

    it(`should calculate direct value and revers`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      userEvent.type(inputFrom, '10')

      expect(inputFrom.value).toMatchInlineSnapshot(`"-10"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+7.26"`)

      userEvent.clear(inputTo)
      userEvent.type(inputTo, '8')

      await waitFor(() => expect(document.activeElement).toEqual(inputTo))

      expect(inputFrom.value).toMatchInlineSnapshot(`"-11.02"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+8"`)
    })
  })

  describe(`USD to UAH`, () => {
    let accountsStore = AccountsStore.create({
      accounts: [
        { currency: 'USD', sum: 100 },
        { currency: 'UAH', sum: 0 },
        { currency: 'GBP', sum: 0 },
        { currency: 'EUR', sum: 0 },
      ],
    })

    it(`should show accountFrom rate`, async () => {
      const { accountFromRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromRate.textContent).toMatchInlineSnapshot(
        `"$1= ₴27,2166"`
      )
    })

    it(`should show accountTo rate`, async () => {
      const { accountToRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountToRate.textContent).toMatchInlineSnapshot(`"₴1= $0.04"`)
    })

    it(`should show accountFrom sum`, async () => {
      const { accountFromSum } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromSum.textContent).toMatchInlineSnapshot(
        `"You have $100"`
      )
    })

    it(`should be empty inputs by default`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(inputFrom.value).toMatchInlineSnapshot(`""`)
      expect(inputTo.value).toMatchInlineSnapshot(`""`)
    })

    it(`should calculate direct value and reversed`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      userEvent.type(inputFrom, '10')

      expect(inputFrom.value).toMatchInlineSnapshot(`"-10"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+272.17"`)

      userEvent.clear(inputTo)
      userEvent.type(inputTo, '8')

      await waitFor(() => expect(document.activeElement).toEqual(inputTo))

      expect(inputFrom.value).toMatchInlineSnapshot(`"-0.29"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+8"`)
    })
  })

  describe(`GBP to USD`, () => {
    let accountsStore = AccountsStore.create({
      accounts: [
        { currency: 'GBP', sum: 0 },
        { currency: 'USD', sum: 100 },
        { currency: 'EUR', sum: 0 },
      ],
    })

    it(`should show accountFrom rate`, async () => {
      const { accountFromRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromRate.textContent).toMatchInlineSnapshot(`"£1= $1.3775"`)
    })

    it(`should show accountTo rate`, async () => {
      const { accountToRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountToRate.textContent).toMatchInlineSnapshot(`"$1= £0.73"`)
    })

    it(`should show accountFrom sum`, async () => {
      const { accountFromSum } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromSum.textContent).toMatchInlineSnapshot(`"You have £0"`)
    })

    it(`should be empty inputs by default`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(inputFrom.value).toMatchInlineSnapshot(`""`)
      expect(inputTo.value).toMatchInlineSnapshot(`""`)
    })

    it(`should calculate direct value and reversed`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      userEvent.type(inputFrom, '10')

      expect(inputFrom.value).toMatchInlineSnapshot(`"-10"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+13.77"`)

      userEvent.clear(inputTo)
      userEvent.type(inputTo, '8')

      await waitFor(() => expect(document.activeElement).toEqual(inputTo))

      expect(inputFrom.value).toMatchInlineSnapshot(`"-5.81"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+8"`)
    })
  })

  describe(`GBP to EUR`, () => {
    let accountsStore = AccountsStore.create({
      accounts: [
        { currency: 'GBP', sum: 200 },
        { currency: 'EUR', sum: 100 },
        { currency: 'USD', sum: 0 },
      ],
    })

    it(`should show accountFrom rate`, async () => {
      const { accountFromRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromRate.textContent).toMatchInlineSnapshot(`"£1= €1,1668"`)
    })

    it(`should show accountTo rate`, async () => {
      const { accountToRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountToRate.textContent).toMatchInlineSnapshot(`"€1= £0.86"`)
    })

    it(`should show accountFrom sum`, async () => {
      const { accountFromSum } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromSum.textContent).toMatchInlineSnapshot(
        `"You have £200"`
      )
    })

    it(`should be empty inputs by default`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(inputFrom.value).toMatchInlineSnapshot(`""`)
      expect(inputTo.value).toMatchInlineSnapshot(`""`)
    })

    it(`should calculate direct value and reversed`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      userEvent.type(inputFrom, '10')

      expect(inputFrom.value).toMatchInlineSnapshot(`"-10"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+11.67"`)

      userEvent.clear(inputTo)
      userEvent.type(inputTo, '8')

      await waitFor(() => expect(document.activeElement).toEqual(inputTo))

      expect(inputFrom.value).toMatchInlineSnapshot(`"-6.86"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+8"`)
    })
  })

  describe(`EUR to USD`, () => {
    let accountsStore = AccountsStore.create({
      accounts: [
        { currency: 'EUR', sum: 100 },
        { currency: 'USD', sum: 100 },
        { currency: 'GBP', sum: 0 },
      ],
    })

    it(`should show accountFrom rate`, async () => {
      const { accountFromRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromRate.textContent).toMatchInlineSnapshot(`"€1= $1.1805"`)
    })

    it(`should show accountTo rate`, async () => {
      const { accountToRate } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountToRate.textContent).toMatchInlineSnapshot(`"$1= €0,85"`)
    })

    it(`should show accountFrom sum`, async () => {
      const { accountFromSum } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      expect(accountFromSum.textContent).toMatchInlineSnapshot(
        `"You have €100"`
      )
    })

    it(`should calculate direct value and reversed`, async () => {
      const { inputFrom, inputTo } = await prepareWidget({
        accountsStore,
        fetcher,
      })

      userEvent.type(inputFrom, '10')

      expect(inputFrom.value).toMatchInlineSnapshot(`"-10"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+11.81"`)

      userEvent.clear(inputTo)
      userEvent.type(inputTo, '8')

      await waitFor(() => expect(document.activeElement).toEqual(inputTo))

      expect(inputFrom.value).toMatchInlineSnapshot(`"-6.78"`)
      expect(inputTo.value).toMatchInlineSnapshot(`"+8"`)
    })

    describe(`input values formatting on change accountFrom input value`, () => {
      it(`should format value="01"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.type(inputFrom, '01')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-1"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+1.18"`)
      })

      it(`should format value="test_string"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputFrom)
        userEvent.type(inputFrom, 'test_string')

        expect(inputFrom.value).toMatchInlineSnapshot(`""`)
        expect(inputTo.value).toMatchInlineSnapshot(`""`)
      })

      it(`should format value="90...101."`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputFrom)
        userEvent.type(inputFrom, '90...101.')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-90.10"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+106.37"`)
      })

      it(`should format value="--1000"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })
        userEvent.clear(inputFrom)
        userEvent.type(inputFrom, '--1000')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-1000"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+1180.54"`)
      })

      it(`should format value="+1000"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputFrom)
        userEvent.type(inputFrom, '+1000')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-1000"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+1180.54"`)
      })
    })

    describe(`input values formatting on accountTo input change`, () => {
      it(`should format value="01"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.type(inputTo, '01')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-0.85"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+001"`)
      })

      it(`should format value="test_string"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })
        userEvent.clear(inputTo)
        userEvent.type(inputTo, 'test_string')

        expect(inputFrom.value).toMatchInlineSnapshot(`""`)
        expect(inputTo.value).toMatchInlineSnapshot(`""`)
      })

      it(`should format value="90...101."`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputTo)
        userEvent.type(inputTo, '90...101.')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-76.32"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+90.10"`)
      })

      it(`should format value="--1000"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputTo)
        userEvent.type(inputTo, '--1000')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-847.07"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+1000"`)
      })

      it(`should format value="1000"`, async () => {
        const { inputFrom, inputTo } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputTo)
        userEvent.type(inputTo, '+1000')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-847.07"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+1000"`)
      })
    })

    describe(`exchange`, () => {
      beforeEach(() => {
        accountsStore = AccountsStore.create({
          accounts: [
            { currency: 'EUR', sum: 100 },
            { currency: 'USD', sum: 100 },
            { currency: 'GBP', sum: 0 },
          ],
        })
      })

      it(`should exchange when type from`, async () => {
        const {
          inputFrom,
          inputTo,
          exchangeButton,
          toAccountSlider,
          fromAccountSlider,
        } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputFrom)

        userEvent.type(inputFrom, '12.35')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-12.35"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+14.58"`)

        expect(
          screen.queryByTestId('ac-currency-exchange-widget-error')
        ).not.toBeInTheDocument()

        expect(exchangeButton).not.toBeDisabled()

        userEvent.click(exchangeButton)

        expect(
          await within(fromAccountSlider).findByText('You have €87,65')
        ).toBeInTheDocument()

        expect(
          await within(toAccountSlider).findByText('You have $114.58')
        ).toBeInTheDocument()

        expect(inputFrom.value).toMatchInlineSnapshot(`""`)
        expect(inputTo.value).toMatchInlineSnapshot(`""`)
      })

      it(`should exchange when type to`, async () => {
        const {
          inputFrom,
          inputTo,
          exchangeButton,
          fromAccountSlider,
          toAccountSlider,
        } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.clear(inputTo)

        userEvent.type(inputTo, '20.50')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-17.36"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+20.50"`)

        expect(
          await within(fromAccountSlider).findByText('You have €100')
        ).toBeInTheDocument()

        expect(
          screen.queryByTestId('ac-currency-exchange-widget-error')
        ).not.toBeInTheDocument()

        expect(exchangeButton).not.toBeDisabled()

        userEvent.click(exchangeButton)

        expect(
          await within(fromAccountSlider).findByText('You have €82,64')
        ).toBeInTheDocument()

        expect(
          await within(toAccountSlider).findByText('You have $120.50')
        ).toBeInTheDocument()

        expect(inputFrom.value).toMatchInlineSnapshot(`""`)
        expect(inputTo.value).toMatchInlineSnapshot(`""`)
      })

      it(`should show validation "insufficient funds on the card"`, async () => {
        const {
          inputFrom,
          inputTo,
          exchangeButton,
          fromAccountSlider,
          toAccountSlider,
        } = await prepareWidget({
          accountsStore,
          fetcher,
        })

        userEvent.type(inputFrom, '200')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-200"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+236.11"`)

        userEvent.click(exchangeButton)

        expect(
          await screen.findByTestId('ac-currency-exchange-widget-error')
        ).toBeInTheDocument()

        expect(exchangeButton).toBeDisabled()

        userEvent.clear(inputFrom)

        userEvent.type(inputFrom, '50')

        expect(inputFrom.value).toMatchInlineSnapshot(`"-50"`)
        expect(inputTo.value).toMatchInlineSnapshot(`"+59.03"`)

        expect(
          screen.queryByTestId('ac-currency-exchange-widget-error')
        ).not.toBeInTheDocument()

        expect(exchangeButton).not.toBeDisabled()

        userEvent.click(exchangeButton)

        expect(
          await within(fromAccountSlider).findByText('You have €50')
        ).toBeInTheDocument()

        expect(
          await within(toAccountSlider).findByText('You have $159.03')
        ).toBeInTheDocument()

        expect(inputFrom.value).toMatchInlineSnapshot(`""`)
        expect(inputTo.value).toMatchInlineSnapshot(`""`)
      })
    })
  })
})
