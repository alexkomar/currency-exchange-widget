import React from 'react'
import { render, screen, within, waitFor } from '@testing-library/react'

import { CurrencyExchangeWidgetRoot } from '../../../components/currency_exchange_widget/currency_exchange_widget_root'

export const renderCurrencyExchangeWidget = async ({
  accountsStore,
  fetcher,
}) => {
  const { container } = await render(
    <CurrencyExchangeWidgetRoot
      accountsStore={accountsStore}
      fetcher={fetcher}
    />
  )

  const fromAccountSlider = await screen.findByTestId(
    'ac-currency-exchange-widget-slider-from'
  )

  const toAccountSlider = await screen.findByTestId(
    'ac-currency-exchange-widget-slider-to'
  )

  return {
    container,
    getDots(mode) {
      if (mode === 'from') {
        screen.debug(document, Infinity)
        return container.querySelectorAll(
          "[data-testid='ac-currency-exchange-widget-slider-from'] .slick-dots li button"
        )
      }
      if (mode === 'to') {
        return container.querySelectorAll(
          "[data-testid='ac-currency-exchange-widget-slider-to'] .slick-dots li button"
        )
      }
    },
    fromAccountSlider,
    toAccountSlider,
  }
}

export const prepareWidget = async ({ accountsStore, fetcher }) => {
  const { fromAccountSlider, toAccountSlider, getDots, container } =
    await renderCurrencyExchangeWidget({
      accountsStore,
      fetcher,
    })

  const fromAccountSlide = await within(fromAccountSlider).findByTestId(
    'ac-currency-exchange-slide-active'
  )
  const toAccountSlide = await within(toAccountSlider).findByTestId(
    'ac-currency-exchange-slide-active'
  )

  const inputFrom = await within(fromAccountSlide).findByTestId(
    'ac-currency-exchange-widget-input'
  )
  const inputTo = await within(toAccountSlide).findByTestId(
    'ac-currency-exchange-widget-input'
  )

  const accountFromRate = await screen.findByTestId(
    'ac-currency-exchange-slide-account-from-rate'
  )

  const accountToRate = await within(toAccountSlide).findByTestId(
    'ac-currency-exchange-slide-account-to-rate'
  )

  const accountFromSum = await within(fromAccountSlide).findByTestId(
    'ac-currency-exchange-slide-account-sum'
  )

  const exchangeButton = await screen.findByTestId(
    'ac-currency-exchange-widget-exchange-button'
  )

  await waitFor(() => expect(document.activeElement).toEqual(inputFrom))

  return {
    container,
    getDots,
    inputFrom,
    inputTo,
    fromAccountSlider,
    toAccountSlider,
    accountFromRate,
    accountToRate,
    accountFromSum,
    exchangeButton,
  }
}
