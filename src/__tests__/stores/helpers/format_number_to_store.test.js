import { formatNumberToStore } from '../../../stores/helpers/format_number_to_store'

describe(`formatNumberToStore`, () => {
  describe(`number value`, () => {
    it(`should format value="12.121234"`, () => {
      expect(formatNumberToStore(12.121234)).toMatchInlineSnapshot(`"12.12"`)
    })

    it(`should format with value="1.00001"`, () => {
      expect(formatNumberToStore(1.00001)).toMatchInlineSnapshot(`"1.00"`)
    })

    it(`should format with value="77"`, () => {
      expect(formatNumberToStore(77)).toMatchInlineSnapshot(`"77"`)
    })

    it(`should format with value="0.077"`, () => {
      expect(formatNumberToStore(0.077)).toMatchInlineSnapshot(`"0.07"`)
    })
  })

  describe(`string value`, () => {
    it(`should format with value=""`, () => {
      expect(
        expect(formatNumberToStore('')).toMatchInlineSnapshot(`""`)
      ).toMatchInlineSnapshot(`undefined`)
    })

    it(`should format with value="00000.1000"`, () => {
      expect(formatNumberToStore('00000.1000')).toMatchInlineSnapshot(
        `"00000.10"`
      )
    })

    it(`should format with value="abcd"`, () => {
      expect(formatNumberToStore('abcd')).toMatchInlineSnapshot(`""`)
    })

    it(`should format with value="+1"`, () => {
      expect(formatNumberToStore('+1')).toMatchInlineSnapshot(`"1"`)
    })

    it(`should format with value="++1"`, () => {
      expect(formatNumberToStore('++1')).toMatchInlineSnapshot(`"1"`)
    })

    it(`should format with value="-+1"`, () => {
      expect(formatNumberToStore('+1')).toMatchInlineSnapshot(`"1"`)
    })

    it(`should format with value="--1"`, () => {
      expect(formatNumberToStore('--1')).toMatchInlineSnapshot(`"1"`)
    })

    it(`should format with value="-9.000"`, () => {
      expect(formatNumberToStore('-9.000')).toMatchInlineSnapshot(`"9.00"`)
    })

    it(`should format with value="9,02"`, () => {
      expect(formatNumberToStore('9,02')).toMatchInlineSnapshot(`"9.02"`)
    })

    it(`should format with value="9,02,0"`, () => {
      expect(formatNumberToStore('9,02,0')).toMatchInlineSnapshot(`"9.02"`)
    })

    it(`should format value="12.121234"`, () => {
      expect(formatNumberToStore('12.121234')).toMatchInlineSnapshot(`"12.12"`)
    })
  })
})
