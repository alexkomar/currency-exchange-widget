import { formatValueInCurrency } from '../../../stores/helpers/format_value_in_currency'

describe(`formatValueInCurrency`, () => {
  describe(`USD currency`, () => {
    describe(`precision="0"`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 12.12, currency: 'USD', precision: 0 })
        ).toMatchInlineSnapshot(`"$12"`)
      })

      it(`should format with value="15.00"`, () => {
        expect(
          formatValueInCurrency({ value: 15.0, currency: 'USD', precision: 0 })
        ).toMatchInlineSnapshot(`"$15"`)
      })

      it(`should format with value="1"`, () => {
        expect(
          formatValueInCurrency({ value: 1, currency: 'USD', precision: 0 })
        ).toMatchInlineSnapshot(`"$1"`)
      })
    })

    describe(`precision="2"`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 12.12, currency: 'USD', precision: 2 })
        ).toMatchInlineSnapshot(`"$12.12"`)
      })

      it(`should format with value="15.00"`, () => {
        expect(
          formatValueInCurrency({ value: 15.0, currency: 'USD', precision: 2 })
        ).toMatchInlineSnapshot(`"$15"`)
      })

      it(`should format with value="1"`, () => {
        expect(
          formatValueInCurrency({ value: 1, currency: 'USD', precision: 2 })
        ).toMatchInlineSnapshot(`"$1"`)
      })
    })
  })

  describe(`EUR currency`, () => {
    describe(`precision="0"`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 12.12, currency: 'EUR', precision: 0 })
        ).toMatchInlineSnapshot(`"€12"`)
      })

      it(`should format with value="15.00"`, () => {
        expect(
          formatValueInCurrency({ value: 15.0, currency: 'EUR', precision: 0 })
        ).toMatchInlineSnapshot(`"€15"`)
      })

      it(`should format with value="1"`, () => {
        expect(
          formatValueInCurrency({ value: 1, currency: 'EUR', precision: 0 })
        ).toMatchInlineSnapshot(`"€1"`)
      })
    })

    describe(`precision="2"`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 12.12, currency: 'EUR', precision: 2 })
        ).toMatchInlineSnapshot(`"€12,12"`)
      })

      it(`should format with value="15.00"`, () => {
        expect(
          formatValueInCurrency({ value: 15.0, currency: 'EUR', precision: 2 })
        ).toMatchInlineSnapshot(`"€15"`)
      })

      it(`should format with value="1"`, () => {
        expect(
          formatValueInCurrency({ value: 1, currency: 'EUR', precision: 2 })
        ).toMatchInlineSnapshot(`"€1"`)
      })
    })
  })

  describe(`BGP currency`, () => {
    describe(`default precision`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 0.01, currency: 'GBP' })
        ).toMatchInlineSnapshot(`"£0.01"`)
      })

      it(`should format with value="999"`, () => {
        expect(
          formatValueInCurrency({ value: 999, currency: 'GBP' })
        ).toMatchInlineSnapshot(`"£999"`)
      })

      it(`should format with value="1"`, () => {
        expect(
          formatValueInCurrency({ value: 1, currency: 'GBP' })
        ).toMatchInlineSnapshot(`"£1"`)
      })
    })

    describe(`precision="0"`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 0.01, currency: 'GBP', precision: 0 })
        ).toMatchInlineSnapshot(`"£0"`)
      })

      it(`should format with value="15.00"`, () => {
        expect(
          formatValueInCurrency({ value: 15.0, currency: 'GBP', precision: 0 })
        ).toMatchInlineSnapshot(`"£15"`)
      })

      it(`should format with value="1"`, () => {
        expect(
          formatValueInCurrency({ value: 1, currency: 'GBP', precision: 0 })
        ).toMatchInlineSnapshot(`"£1"`)
      })
    })

    describe(`precision="3"`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 0.011, currency: 'GBP', precision: 2 })
        ).toMatchInlineSnapshot(`"£0.01"`)
      })

      it(`should format with value="999"`, () => {
        expect(
          formatValueInCurrency({ value: 999, currency: 'GBP', precision: 2 })
        ).toMatchInlineSnapshot(`"£999"`)
      })

      it(`should format with value="10"`, () => {
        expect(
          formatValueInCurrency({ value: 10, currency: 'GBP', precision: 2 })
        ).toMatchInlineSnapshot(`"£10"`)
      })
    })

    describe(`precision="4"`, () => {
      it(`should format with value="12.12"`, () => {
        expect(
          formatValueInCurrency({ value: 0.01, currency: 'GBP', precision: 4 })
        ).toMatchInlineSnapshot(`"£0.0100"`)
      })

      it(`should format with value="999"`, () => {
        expect(
          formatValueInCurrency({
            value: 999.0012,
            currency: 'GBP',
            precision: 4,
          })
        ).toMatchInlineSnapshot(`"£999.0012"`)
      })

      it(`should format with value="1"`, () => {
        expect(
          formatValueInCurrency({ value: 1, currency: 'GBP', precision: 4 })
        ).toMatchInlineSnapshot(`"£1"`)
      })
    })
  })
})
