import { types } from 'mobx-state-tree'
import { formatValueInCurrency } from './helpers/format_value_in_currency'

export const AccountStore = types
  .model({
    currency: types.string,
    sum: types.number,
  })
  .views((self) => ({
    get precision() {
      return (self.sum * 100) % 100 === 0 ? 0 : 2
    },
    get formattedSum() {
      return formatValueInCurrency({
        value: self.sum,
        currency: self.currency,
        precision: this.precision,
      })
    },
  }))
  .actions((self) => ({
    addSum(diff: number) {
      self.sum = self.sum + diff
    },
  }))

export const AccountsStore = types.model({
  accounts: types.array(AccountStore),
})
