import currencyFormatter from 'currency-formatter'

export function formatValueInCurrency({
  value,
  currency,
  precision = 2,
}: {
  value: number
  currency: string
  precision?: number
}) {
  return currencyFormatter.format(value, {
    code: currency,
    precision: (value * 10**precision) % (10**precision) === 0 ? 0 : precision,
    format: '%s%v',
  })
}
